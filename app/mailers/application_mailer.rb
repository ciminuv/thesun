class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@ess.vn'
  layout 'mailer'
end
